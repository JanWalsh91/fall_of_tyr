# Leucis

Race | Class | Background | Age
-|-|-|-
Human Tiefling | Fighter | Urchin | 25

## Overview

- Leucis was abandonned by his parents at an early age in Amlon.
- He lived on the streets and sufferred many hardships until he came of age and moved to Mt Maggrom for a better life.
- He was forced to live as a thief out of necessity, though he dreamed of redeeming himself and joining the city guard and having a noble profession. 
- Traumatized from experiences in his youth, he felt that city life was not for him, and left the city, finding Arcata.
- The Firblogs of Arcata took him in, and helped him keep his mind at peace, helping occasional strangers who pass through the woods.

## Personality Traits 

- Overemotional: Excessively or abnormally emotional. Sensitive about themselves and others, more so than the average person.
- Absent-minded: Preoccupied to the extent of being unaware of one’s immediate surroundings. Abstracted, daydreaming, inattentive, oblivious, forgetful.
- Hedonistic: Pursuit of or devotion to pleasure, especially to the pleasures of the senses.

## Ideals

- YOLO: who knows when we'll die and what happens after. So let's make the most of it.

## Bonds

- His life has changed a lot since he arrived in Arcata, and now calls this place home. He is fond of the Firblogs who took him in and those who take care of him.

## Flaws

- He's tried to let go of his past so much that he has lost a sense of purpose in life. He is sometimes overly skeptical about doing things because he fails to see a higher purpose.

## Goals