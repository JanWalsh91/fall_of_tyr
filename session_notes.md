# Session 0

## Maggrom

- Central hub.
- Trading hub
- Tunnels under the mountains
- Halfling run mount Maggrom


Trade shut down 50 years ago, mainly due King of Amlun, Ulrich. Steward has taken care of the throne since 50 years. Old person. Everyone trying to get a claim to throne. Diplomacy broke down. All kingdoms suffered drastically due to lack of magic.

Mount Maggrom suffered the least. Had huge stockpiles. Halflings are crafty tradesmen. Halfling throne somewhat forgiven. Other cities end up trading with them. 

Maggrom Mountains to the north maggrom

Bottom: workers, tradesmen, everyday folk.
Middle: markets, producers
Top: Rich merchants partiers. Administrators.

## Amlon

Seat of the throne, seat of diplomacy until Ulrich passed.

## Blackstone

Reclusive. Rarely see inside. Outside of the gates, live the poorest of the poor. Some farming. Generally isolationist. During days of trade, some ambassadors and diplomats and large trade caravans are allowed inside, though, generally, caravans stop outside. Defensive grumpy lot.

## Eastborne

Has been able to put up the facade of doing fine, thanks to magic, especially. Keep a good lifestyle. Good at acting as if nothing happened. Were indecisive about who to join, dwarves or humans. Are probably the most open to reopen their gates first. 

Average elf is more or less like a human.

## Pantheon

Religion is not common. Exists, but no major churches. Religions are seen as cults. No one really believes in gods. Some believe, but are seen as gullible. 

Can make up your own gods. 

One creationist story that gathered more traction: believe that the world was created when the minds of two great being collided. The impact of that is what created the matter that stared this world. Stories vary a lot after that.

Surrounding this continent, a storm. Everyone know this. There is always a storm at the horizon. Most ships and flying creatures can't go there. Wizards think it's magical. Most people just ignore it.

Forgotten realms pantheon. Belief in Tyr. Spiritual manifestation in the continent.

Bad pantheons exist too. Eg satanic cults. 

## Notes on Races

- Orcs: brutish, savage
- Goblins: nuisances, dangerous in a pack, rat-like otherwise
- Elves: Small family size. 1.5 children. 
- 
## Travelling and Communication

Sending stones are commonly used by the rich
Travel: horse and car, tried flight without wind. Burgeoning technology by gnomes. 
Teleportation Circles: very much a thing

## Gender divide

What you'd expect compared in 1900's. Women adventurers are less common. Separated in bathhouses. Breaking gender boundaries. Artificing more tailored towards men. Mass produce magical items. Brothels: both genders. 

## Law

Guards will always be our level.

## Adventuring

Never been an adventuring guild. No cohesive unit. Average: bring back food for village. Escort caravan.

## Monastery of Flowers

Tieflings are seen are touched by the devil. No one trusts or likes them. Best to send them to the monastery to be cared for by monks. Also, mentally broken people. Can live there comfortably. Beautiful location. Tieflings rarely get handed over by their parents. 

Deep gnomes are odd little brothers who hide in their caves and don't want to come out.

## Background

Each one of us has performed a notable action which gained the attention of a patron. You're all being invited to partake in the building of a franchise. This man wants us to bring magic items to reopen trade. 

Role additional gold based on background

Drow - don't raid the overworld. Dirty secret they exist there. Still reside in underground. Not massively populated. Live in series of underground caves and towns underneath Eastborne. Spells that stop them from coming out. Entrance in Eastborne. 

Originally, lived in caves in the Undercity. No plants, and animals. Fungi. Your character formed a bond with one of the pure elves up above, and helped me escape at a young age. Wouldn't know what to think. In Eastborne, there will be questions, about how I got out of here. Point of reference. Shooed me out. Could have been forced back into the Undercity. Create npc.

Sunlight sensitivity. 60ft darkvision. Only disadvantage on perception checks. 

Some mage, was the one that got me out. Hid me and trained me. Not archmage, but somewhere in hierachy. When they found me with tome, and having noticed my attunement. Pre judgement: another dark elf doing bad things.

## Creation

- level 5
- 1 feat
- 1 ASCI
- 27 point buy system
- add racial stat bonuses
- background
- pick standard starting equipment 
- gold: 110g



# Session 1 - 2020.03.08

Went through puzzle diorama and killed Mr Grumps.

Black Hand: symbol of main trade organization before trade was shut down. Focused on trade guarding and were unscrupulous about clients. Now taken over by Guild of Shields.

Warehouse: large hulking stone figure. Rickety structure at edge of a cliff. Near empty street. 

They were known as Black Hand

Acquire goods. Go out to seek contracts. 

10 for 10 arrangement. Case of operations: this warehouse.
10 percent for 10 percent. They take 10 percent. They give 10 percent of items sold.

Hard to get a hold of Mr B.
Hethrows has a sending stone to contact him. He is the face of the company.

Will cover other employees.

Signing the contract: you have to choose a role. Does not cover the costs of damage.

Golden Goose bar near the warehouse.

Don't come to warehouse between 9 to 11 am.

Basement of the warehouse, same size. Bunk room. Small cooking hole. 

Mr Veeie: old and faded but very fine robes. Builds trinkets, wands, mundane and arcane items. Under his hood, two starlight pin points of eyes. Has a skull for a face, entire body is skeleton. He's a lich.

Flock and Tallier: scroll and potions on second level.

# Session 2 - 2020.03.15

Bottle of Dry White Wine

Phin:
- Has horns covered by horn

Contract - very flowerly written. Items of particular interest will be given to the franchise. 

King's Bay. Politicized cities. Trading hub. Social unrest. No trade left. Just a fishing port.

Party Invitation
- Upper Ring


Reginald Wodsworth, butler. 

Jeremiah Halsworn, halfling.

Has gambling problem. Rumors that he doesn't have money, but lives lavish lifestyle. I was sent to find out how he pays for his lavish lifestyle.

Friends:

Shana, elven woman.
Shana / Jahna Eldragim
Henry Boulger, human, friendly. Silent.
Arnon, Aldagrim Roper, King of Maggrom, halfing.

Died. He was poisoned. Letter is blank.

Mr Veeie wants a rat.

Jeremiah (Brother of Jeremiah) - Janah Eldergram marriage

# Session 3 - 2020.03.29

Jeremiah's house. One of the guests slid out and was replaced by their twin. She was going to marry one of his relatives to collect on inheritance and bail. We got in the way and fought. We are not sure which of the twins we have. Twins: Shawna or Jana. 

Bulter and Arnon outside. Henry disappeared. Perhaps with the body.

Arnon doesn't know Shawna or where she lives. 

Got financial documents and broken watch.

Arnon wanted to get an ornate cypher.

Bring the Twin back to the warehouse and bind her. Investigate the documents:

- main folder: finances: all in the red. Shock that he had money to gamble in the first place. He should not have had money.
- Extra paper: letter for his brother about woman Janah about how he was in love and were planning to get married. Asked for financial help for the wedding.
- Receipt: (one you keep as business). Handwritten in Jeremiah's handwriting. Describes payment for services performed at 2:34 am in the morning, dated several months ago. Services provided by Henry Bolger. Both signature. 2000 gp.
- The broken watch stopped at exactly 2:34 am.
- In the finances, after the date of the receipt, his finances spirals downward.
- Some payments towards the Guild of Shield

Veseryn known that Henry is a necromancer.

Silver pocketwatch on chain. Engraved with flowery patterns on outside. Outside is scuffed as if dropped. CHain broken as if it was tugged off from someone's neck. Jeremiah's initials inside. Cracked dome face is broken. Cogs are broken. 

One statue next to pocketwatch reminded me of my youth.

She says she is Shana. She claims to have assisted in the murder. She made sure to be invited so that her sister could be there to kill Jeremiah.

She confesses to have stolen apple tarts when she was six.

She has receipts. Dress maker for adjustments to her clothes. Herbalist for Sourgrass. It's weed.

The guards don't think he has a brother.

Jebb is concerned about Mr B. Hasn't heard from him in a while. Jebb replaced toad with Mr Smiles.

Dog is necromantic.

Veiee presents a sac with sending stones. Small. inch across. Flat with a hole in the center. All linked together. Only communicate with each other. Range: 1 mile. Hold it in hand, speak into them, at least whisper.

Crucifix, straps rat to it, and lowers a electro shock helmet on its head.

Librarian Asmodus - is a name in divination.

Dog name brainstorm:
- Undoggy
- Bones
- Mr Bones
- Fester
- Hades
- Deadeyes

Evelyn learned that Jeremiah does not have a younger brother. At least the guards believe he doesn't exist. Jeremiah ows the guard serious amounts of money. In dept to many groups.

Richard Mistleheart - Guild of eyes member, primary role as a go between for members to contact and hand information to. He would be the one members meet to discuss aid, equipment, and goals when it comes to their current or future missions. 

Speaker for the council of eyes, the group of guild leaders that rarely meet members in person, Richard is the one that funnels  information in and out for them.

He is most often found playing for bars in the mid tier of Maggrom, an expert with a lute, and a keen ear to listen in on conversations between songs. He is adept at spotting the right person for the right subtle word.

# Session 4 - 2020.04.05

Collected some leather pelt.

Mr Veiee's last name is Phantalier.

Evelyn changed her name to Evaline Phantalia.

Wake up to a scream. Someone is attacking Mr Smiles.



Library on second level, on way up to top. Receeds into mountain. 


Asmodeus: Chunky Haflfing. 3ft

They come at night. The imp in the Occulus. A flash of light appeared then the book is removed.

Common books: all about magic:
- How to control innate magic
- Plights of dealing with patron
- How to for dummies
  

- A symposium on the Origin of Sorcerers by Abbey Hawk.

- The Marble Tower of Malich

The guild of Eyes have a home meeting UNDERNEATH the Tavern called Harden's Garden. Due to the transitory nature of the guild they don't have a hall or anything, as they are more spread in smaller nodes and each group meets wherever is best at the time, be that taverns or otherwise in short bursts. 

The council however has a constant location underneath Harden's Garden, and this is where you would likely meet Richard Mistleheart most if not everytime as he speaks for the council beneath the building

The Silver Feather, a casino on the upper ring of Maggrom, a place where Halsworn went to gamble.

# Session 5 - 2020.04.19

Gronch ate the frog, and slept off the poison.

Kenku points to Mr F. He works for a franchise. He does not know Mr B.

His tome is a on the origins of wild magic and how most people with wild magic end up. May have gotten his powers by chance.

Know where Mr F is. EHF.

Waste End of the middle ring. Where merchants dispose of things. Only slightly nicer than the bottom ring.

Other franchise: Iceberg in water. Invitation sheet serves as a swipe card to get into the franchise through a brick wall. 

Discovered a room of skeletons ignoring us and writing pages of books in common. The steal books, copy them and bring it back to library.

We find:
- Cloak, weighted
- Rapier with wings
- Dagger with snake pattern. Daggers of venom. +1. Once a day, poison dmg.
- Necklace: Prayer Beads. 6 Beads. 5 yellow. 1 blue.
- Nice steel Shovel. Engraving with gravedigger ferrying lost souls.
- Ivory axe, steel head. Fang protrusions from the back.

- I found: 
- 5 torches
- Climbing gear, hammer, pitons, 50 foot of rope.
- 19 copper
- 5 silver
- 5 gold

When the lich 'died" it said "The Boney Boys will never die"

Potentially increased profit, but perhaps may have cost something else.

Coffin broken because something broke out. Inside, one bottle of unknown liquid, 16 copper, 1 silver, 12 gold.

4 pieces of half plate. 750 each.

Arrive at beautiful graveyard on top level.

Plume of fire smoke from Halsworn's Estate.

Along the side of the estate, in the shadow, there is a small halfling figure, in full cloak, Henry Bulgar talking.

"When you open it, you will understand."

Someone handed me the cryptex.

Took the Rapier. 

The spider that killed Jeremiah Halsworn:
Green Weaver - Common Underdark spider - Highly venomous yet considered non lethal to the drow.

The Green Weaver upon biting injects a poison that immediately paralyses, and within a minute can kill the unprepared.
Though it is only found in the Underdark, and without careful enclosure and delivery will die outside in the sunlight, upon death they crumble to ash leaving little trace of their existance.

The drow consider these a mere inconvinience, mainly because due to prolonged exposure to the same enviroment they seem unaffected by the poison for the most part, a Green Weaver bite to a drow is a painful sting, and only in the cases of rare allergy would a death be the result.
The Green Weaver is a small creature no larger than a 50 pence coin in span, bearing a small body compared to the reach of its thin legs. It resembles a Noble False Widow though its colouration is solid black with a pearlescent green layer in the right light. It produces near excessive amounts of "Weave" a type of spider silk renowed for its strength even compared to the norm. Hence its name, the Green Weaver.

# Session 6 - 2020.04.26

- get someone to look at the liquid

Weather is very cloudy, dark.

Dog: Mr Brigsy Bones.

Vesryn sees figure on the way back to warehouse.
Humanoid. Short black hair. Gruff square jaw. Dressed like a scribe. Rough demeanor.

After we follow him a bit, he ducks into an alley way, in the industrial area. Talks to two members of Guild of Shields. "I lost them, keep an eye on the warehouse". Gronch grabs him in a sac.

Back to warehouse. Jeb will take care of lose ends.

Jebb lives in his office. 

Evelyn, Gronch and Vesryn are interrogating the scribe guy who followed us.

We are suspect of murder and now arson.

Evelyn tries to pay him off with 100g. But in the end, Jeb says he'll take care of it.

Veiee is injecting something into rat.

During the night, hear screams and bone breaking.

In morning, Jebb told us that he wasn't very forthcoming. He was a guard. He has been hired to 

Mr B will come visit soon.

Clear liquid when it moves. Strange light blue color when it stops moving. In a still state, it is thinner than water.

Strange dream with sword at a dance. The sword joined me in the dance. Sword: https://www.dndbeyond.com/magic-items/dancing-sword 

Next day. 

Heavy rain. No lightning.

Sunshine is an approximation of his name. 21 years old. mature at 12.

Payed 7g3s for various items (vials, chalk, fishing line, tinder box, mirror, tanning resources)

Gronch gave a child a dagger to hunt goblins. Gronch reads a story about tower and village to Vesryn. Gronch. His goblins all disappeared one day.

The drawing is of a small collection of buildings set around a larger central building. Town hall. In the background, you can see a large square tower with a bell in it and a spire on each corner.
Need to find this tower. His people disappeared in a tower. 

Lightning hits on top of the warehouse.

A phylacetry can be anything from a pebble to a building. Must be something that has been shaped. Phylactery should be located somewhat close, at most, a mile.

Look for chunky rats.

Cryptex: 6 dials: 26 symbols.

No maker's mark. Engravings around it are star connected by lines that go all the way around.

Evelyn buys a winged serpent. White, spotted with blue scales. 2 foot.

The Tyr Home World Stars constellation on the left of the cryptex, the north star. If you follow it, you will always find your way home.
The other one is the Under World Stars. Exact opposite pattern. Star that the dead follow to their resting place.

In Eastbourne: the Tyr star is called the Dreaming star, the other one is the Nightmare star. Both stars are seen as bad omens. Dreaming thought as a way for things to get into your head.

Mr B: Man with robes, exact same robes as Mr Veiee's. Aged and red robes with silver trim. moving as if on hoverboard enters the warehouse. He's a Lich.

Phin identified the bottle of unknown liquid: is a potion of invisibility. 

Mr B is trying to craft a franchise to affect the world for the better: to reopen the gates. May be able to convince some groups to open up again. He wants trade to open again. He has known Mr V for centuries. Jebb is a solid friend. He has seen this cryptex before. There is only one of these. The code has probably changed since.
Task: open your first gate. Travel to Eastbourne, Amlon, or Blackstone, and try to convince them to open trade with Maggrom. Will give us a sending stone to contact Jeb once a day. Won't be limited by range. Can prepare for up to a week. Will offer us: an bottle of silver black liquid: liquified memory. His. Take a sip before we sleep and we will learn something. 
Since they all lost their names, they decided it was easier to just name themselves.

Mr Veiee has been under stress. 

Tell Mr Veiee that "The 26 wait"

We are the fifth team. 

Dream: large cavern. Air light by dancing light spells. 26 seats. You're standing behind them. the seats are in a line against a  mirrored wall. Not all the seats are occupied.

B: "We are losing numbers, we need to stop splintering and stay together as a group"

Some are wearing armor. You can see Mr F.

F: "Be damned with the sleeping giants. I am going to leave and form my own"
B: "I understand you miss your fiend but hhe stepped down of his own accord, but Bulgar did what had to be done. Try to bring Try back together."