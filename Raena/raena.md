# Raena Galanodel

Galanodel (Moonwhisper)

Race | Class | Background | Age
-|-|-|-
High Elf | Artificer | Sage | 70

## Overview

- Born and raised in Eastbourne.
- Born in a middle class family.
- She's an artificer who works primarily for nobles in Eastbourne and on occasion for foreign customers.
- She has a good reputation and is often sought after for medical draughts, magical trinkets and enchantments.
- She is interested in Dark Elves and their history, and often considers their potential for being good and doing good.
  - She does not publicly and politically share this idea as she does not want to risk her reputation.
- She regularly goes outside of the city to forage for her potion-making. 
- She occasionally travels to places father away for archeology research, recovering ancient artifacts, discovery and study of plants and animals. She likes to bring back tokens from her travels to add to her collection. Her good reputation among nobles allows her to receive grants for her research and travels.


## Personality Traits 

- Raena has a caring and nurturing personality, but is contrasted with bouts of selfishness and impulsive decision-making.
- She is polite, well-spoken and eloquent.
- She is able to be serene, even in the face of disaster.

## Ideals

- Knowledge is the source of power and self improvement
- One must preserve face at all costs

## Bonds

- She considers herself to be Morwen's adoptive mother and is proud of her even though she was forced to cast her out of the city.

## Flaws

- Raena will put other people down in order to uphold and protect her reputation. She is willing to sacrifice things she loves for her reputation.
- Selfish and occasionaly impulsive

## Goals

- Short Term:
  - She is planning a new expedition in hopes of finding a rare plant with special medicinal properties. She hopes to find what she seeks.
- Long Term:
  - She wants to be considered a noble and be fully accepted by the Eastbourne nobility.