# Morwen 

## Name

Morwen (Dark Maiden) Galanodel (Moonwhisper)

Pseudonym: Sylraena (Elf haven)

Race | Class | Background | Age
-|-|-|-
Drow | Warlock | Guild of Eyes | 30

## Relationships

- Mother - unknown
- Father - unknown
- Brother (younger) - unknown
- Teacher Raena Galanodel
- Richard Mistleheart - liaison with the Guild of Eyes

## Overview

- I escaped the Undercity at age 5, and was rescued and taken under the wing of Raena Galanodel, an artificer who works for various nobles in the city.
- During my childhood Raena instructed me in various fields of study like history, alchemy, magic, and biology. Though I was fascinated and engaged, I failed miserably.
- at age 11, Raena began to let me leave the house under cover, either at night by hiding my face, or during the day with an enchantment to conceal my true appearance, to help her forage for ingredients. I spent many nights dancing in the woods.
- at age 17, I discovered the magical Tome "Of Knowledge and Deep Desires" hidden away in one of Raena's vaults. Reading the Tome gave me insight and Clarity of Knowledge which aided me in my magical abilities. To my horror, its prolonged use magically scarred my body.
- at age 19, I was caught communing with the Tome by a city politician, unknown to me, who then divulged my secret to Raena and, for the sake of her reputation and her position, was convinced to have me leave the city. Raena was disappointed in me, but we left on good terms. Though she hid the Tome away from me, my cemented connection with it allowed me to summon it after my departure.
- Disoriented and helpless, I found a small reclusive Firblog community called Arcata. They take me in and bind my wounds with magically imbued leather.
- There I also met Leucis, a Tiefling, another outsider. We became close friends. Leucis's experiences and life in Arcata seeded my sense of compassion for all outsiders. This reignited by connection with my Dark Elf identity.
- at age 21, I headed for Mt Maggrom, hoping to gain the knowledge and power necessary to reunite the elves and free my people.
- I joined the Guild of Eyes; they saw my potential as a spy and infiltrator.

## Memories

### Todo: 

- Failed lessons with Raena
- Dancing in the woods after foraging.
- First discovery and communing with the tome. 
- Conflict between rashes and benefits of the tome.
- Getting caught by Raena communing with tome.
- Saying goodbye to Raena, and leaving Eastbourne

## Stats

_ | STR | DEX | CON | INT | WIS | CHA | Max HP
-|-|-|-|-|-|-|-
Point Buy | 8 | 14 | 14 | 10 | 10 | 15
Dark Elf | 8 | 16 | 14 | 10 | 10 | 16 | 10 (8+2)
Level 4 | 8 | 16 | 14 | 10 | 10 | 18 | 31 (10+(5+2)*3)
Level 5 | 8 | 16 | 14 | 10 | 10 | 18 | 38 (38+(5+2))
Level 6 | 8 | 16 | 14 | 10 | 10 | 18 | 45 (45+(5+2))


## Spells and Abilities

Level | Spells 
-|-
__Cantrips__ | Eldritch Blast, Message, Dancing Lights, Thaumaturgy, Mending, Minor Illusion, Toll the Dead, Prestidigitation, Mage Hand
__1__ | Hex (C), Armor of Agathys, Alarm (R), Detect Magic (R), Healing Word, Longstrider, Feather Fall, Silent Image
__2__ | Invisibility (C), Misty Step 
__3__ | Hypnotic Pattern, Fear
__4__ | 
__5__ | 
__Invocations__ | Mask of Many Faces, Agonizing Blast, Book of Ancient Secrets
__Pact__ | Fey Presence

<br>
<br>


## Progression

- Level 1:
	- Free Feat:
		- War Caster
	- Guild of Eyes:
		- Proficiency:
			- Deception
			- Stealth
		- Proficiency:
			- Thieves Tools
		- Languages:
			- Thieves Cant
		- Equipment:
			- gaming set (cards)
			- lucky charm
			- set of fine clothes
			- belt pouch with 15gp
	- Elf:
		- DEX +2
		- 30 ft speed
		- Darkvision 60 ft
		- Proficiency:
			- Perception
		- Advantage on saving throws against being charmed. Magic can't put you to sleep.
		- Trance
			- Can meditate 4 hours instead of sleeping
		- Languages:
			- Common
			- Elvish
	- Dark elf:
		- CHA +1
		- Superior Darkvision 120ft
		- Sunlight Sensitivity: disadvantage on attack rolls and WIS (Perception) checks when you or target is in direct sunlight.
		- Drow Magic:
			- 1 Cantrip:
				- Dancing Lights
		- Elf Weapon Training:
			- Proficiency:
				- Longsword
				- Shortsword
				- Shortbow
				- Longbow
	- Warlock:
		- Proficiency:
			- Light Armor
			- Simple Weapons
			- Nature
			- Investigation
		- Equipment:
			- simple weapon: blowgun + (20 needles)
			- components pouch
			- Scholar's Pack:
				- backpack
				- book of lore
				- bottle of ink
				- ink pen
				- 10 sheets of parchment
				- bag of sand
				- small knife
				- 40gp
			- Leather Armor
		- 2 Cantrips:
			- Eldritch Blast
			- Message
		- 2 Level 1 Spells:
			- Hex
			- Armor of Agathys
		- Archfey Pact Boon:
			- Fey Presence: 
				- As an action, you can cause each creature in a 10-foot cube originating from you to make a Wisdom saving throw against your warlock spell save DC. The creatures that fail their saving throws are ali charmed or frightened by you (your choice) until the end of your next turn. (CD short rest)
- Level 2:
	- Warlock:
		- 1 Level 1 Spell: 
			- Cause Fear
		- 2 Invocations: 
			- Mask of Many Faces
			- Agonizing Blast
- Level 3:
	- Dark Elf:
		- Drow Magic:
			- Can can Faerie Fire 1 / day  
	- Warlock: 
		- 1 Level 2 Spell:
			- Invisibility
		- Pact Boon:
			- Pact of the Tome:
				- 3 Cantrips from any class's spell list:
					- Thaumaturgy
					- Mending
					- Minor Illusion
- Level 4:
	- ASI: 
		- CHA +2
	- Warlock:
		- 1 Level 2 Spell:
			- Misty Step
		- 1 Cantrip:
			- Toll the Dead
- Level 5:
	- Dark Elf:
		- Drow Magic:
			- Can can Darkness 1 / day
	- Warlock:
		- 1 Level 3 Spell:
			- Hypnotic pattern
			- Forget cause fear, learn Fear
		- 1 evocation:
			- Book of Ancient Secrets:
				- 2 1st level ritual spells from any class. Can only cast those spells as rituals. Can add new ritual spells to the book:
					- Alarm
					- Detect Magic
- Level 6:
	- Bard (level 1)
		- 1 skill
			- Sleight of hand
		- 1 musical instrument
			- Satyr Pipes
		- 2 Cantrips
			- Prestidigitation
			- Mage Hand
		- 4 Known Spells (level 1)
			- Healing Word
			- Longstrider
			- Feather Fall
			- Silent Image
		- 2 Level 1 Bard Spells slots
		- Bardic Inspiration