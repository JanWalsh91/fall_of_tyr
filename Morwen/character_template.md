# Morwen

## Physical Description

Morwen is a slender dark elf with obsidian skin and long straight pearl colored hair. She usually wears a grey cloak over her body and head, with a veil covering her mouth. Under her cloak, her body is wrapped in intricate leather straps carved with glyphs that glow purple when spellcasting. This leather armor covers most of her body including her wrists, ankles and neck. Underneath, her body exposes a myriad of faintly glowing scars, one of which goes through her face and left eye. While her right eye retained the vibrant forest green of youth, this one has turned black.

Morwen often disguises herself magically by rendering her leather armor invisible and changing the color of her cloak, skin and hair. In Mt Maggrom, her identity is that of Sylreana, a high elf with cream colored hair and fair skin. Morwen does her best to conceal the scar through her eye, but often, the best she can do is conceal it as a dark purple vein.

Left on her own, Morwen tends to hum tunes, and dance.

Her quicks include checking herself in the mirror often and fidgeting with her hair.


## Personal Relationships

- Born: Morwen (family name unknown, but uses Raena's family name: Galanodel). Currently goes by Sylraena or other aliases if under cover.
- Father: unknown 
- Mother: unknown 
- Siblings: one younger brother – unknown
- Teacher and maternal figure: [Raena Galanodel](../Raena/raena.md)
- Friend: [Leucis](../Leucis/leucis.md)
- Liaison with Guild of Eyes: Richard Mistleheart 	

## Bio (Written History)

- I escaped the Undercity at age 5, and was rescued and taken under the wing of [Raena Galanodel](../Raena/raena.md), an artificer who works for various nobles in the city.
- During my childhood [Raena](../Raena/raena.md) instructed me in various fields of study like history, alchemy, magic, and biology. Though I was fascinated and engaged, I failed miserably.
- at age 11, [Raena](../Raena/raena.md) began to let me leave the house under cover, either at night by hiding my face, or during the day with an enchantment to conceal my true appearance, to help her forage for ingredients. I spent many nights dancing in the woods.
- at age 17, I discovered the magical Tome "Of Knowledge and Deep Desires" hidden away in one of [Raena](../Raena/raena.md)'s vaults. Reading the Tome gave me insight and Clarity of Knowledge which aided me in my magical abilities. To my horror, its prolonged use magically scarred my body.
- at age 19, I was caught communing with the Tome by a city politician, unknown to me, who then divulged my secret to [Raena](../Raena/raena.md) and, for the sake of her reputation and her position, was convinced to have me leave the city. [Raena](../Raena/raena.md) was disappointed in me, but we left on good terms. Though she hid the Tome away from me, my cemented connection with it allowed me to summon it after my departure.
- Disoriented and helpless, I found a small reclusive Firblog community called Arcata. They take me in and bind my wounds with magically imbued leather.
- There I also met [Leucis](../Leucis/leucis.md), a Tiefling, another outsider. We became close friends. [Leucis](../Leucis/leucis.md)'s experiences and life in Arcata seeded my sense of compassion for all outsiders. This reignited by connection with my Dark Elf identity.
- at age 21, I headed for Mt Maggrom, hoping to gain the knowledge and power necessary to reunite the elves and free my people.
- I met Richard Mistleheart at a tavern. We shared our interests, music and dance. He invited me to perform with him on several occasions. A few times too many, I noticed him listen in on various conversations at taverns. He eventually revealed that he was operating under cover. He confessed not only that he belonged to the Guild of Eyes, but he and the council saw my potential as a spy and infiltrator. Interested in finally gaining a position of power in this city, I joined the Guild.
- I became a trusted member of the Guild, maintaining my position as a spy and infiltrator notably by participating in exclusive dance performances for the rich and powerful. Though I've been in some dangerous situations, I've not been caught, or have to engage in any direct violence.
- I dance at night in the hanging gardens on my own during the night, where no one can see me, as a emotional release.
- In the evenings, I give performances at The Rolling Boulder, a Tavern and Casino in the top ring of Mt Maggrom. My current job is to collect information on Jeremiah Halsworn, a gnome who turned up with no real history and apparently endless pockets.
- Recently, walking the streets of Mt Maggrom, I saw a boy being threatened by a bully. I created a minor illusion to give the boy fiendish red eyes, which scared off the bully.

## Goals

### Short Term Goal:

- Help outsiders like myself as well as the innocent and the helpless.

### Mid Term Goal: 

- Learn about elven history and the history of the Dark Elves

### Long Term Goal: 

- I want to reunite the Elves, free the Dark Elves and find my family.

## Secrets

(pick a couple or more things that your character might not want to divulge, or dirty past actions, you know the drill)
- I hate the scarring on my body and don't want others to see it.
- I have to be careful about who I reveal my Dark Elf identity to.

## Strong Ties

(Pick a few things or people that your character has strong connections to, or strong emotions about)
- I keep in touch my mail with [Raena](../Raena/raena.md) and my friend in the Firblog woods.
- I hope to use the Guild of Eyes to find and correct injustices wherever I may find them.

## Notable Information 

(Note down anything especially important about your character, or their beliefs/morals)
- I feel strongly about people who are cast as outsiders of a community and strive to defend them.
- I am very concerned about my appearance and will do what I can to disguise myself and deflect when the topic comes up.


Additional Info
===============

## Personality Traits 

- Hopeless romantic
- Nothing can shake my optimistic attitude

## Ideals

- I always try to help those in need, no matter the cost
- There's a spark of good in everyone.
- Art should reflect the soul; it should come from within and reveal who we really are.

## Bonds

- The Firblogs that took me in.
- [Leuci](../Leucis/leucis.md)
- [Raena](../Raena/raena.md)

## Flaws

- I can't resist an innocent and pretty face
- I will pay a high price to gather important knowledge

## Spell Reskin

Morven's armor was given to her from the Firblogs of Arcata and is one of her most valuable possessions, matched only by her Tome. The leather armor is most peculiar. It looks like a strap of leather was tightly wrapped and weaved around her body. The dark rich brown straps are encrusted with bright purple glyphs. Morwen uses the armor when she casts spells. Her Eldritch Blast summons long tentacle-like straps which dash out and whip her enemies. Her Hex sends out leather bands to wrap around her target. 