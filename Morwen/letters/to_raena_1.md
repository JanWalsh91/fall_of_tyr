# Letter to Raena Galanodel

Dear Mother, 

It's been a while since I received word from you, but my situation has changed in recent weeks and felt obliged to provide you with news. I assume you have left on one of your expeditions once more. I am disheartened that you've not informed me, but not surprised.

I have discovered promising new business opportunities in which my skills are valued. And no, Mother, you'll be glad to hear it's not part of the industry I worked in previously. In fact, I have been selected, among others, to provide various detective work and protection services around Mt Maggrom. We are hoping to gain renown in this city, and hopefully, the region. The business is actually an old guild that existed before the Fall, and was called the Black Hand. Have you come across the Black Hand in your historical studies?

Speaking of studies, who would have known that one of our missions would take us to a library, of all places! That got me thinking, perhaps the libraries here could hold important information about our history. Perhaps they contain books that *some* libraries keep under lock and key.

Dancing and performing and making connections with people will always be an important part of me; I'm sure I will miss being on stage every night. But our missions with the Black Hand are far from dull. In fact, we were almost witness to a murder committed in the room above where we were, well, safeguarding the person who was murdered, albeit, we were distracted by card games... But I digress. This brings me to an important matter I wished to ask you. We discovered that two light elf women by the names of Shawna and Jana were complicit in the crime. Their weapon of choice was death by a Green Weaver. How these women managed to get one from the Underdark to here in Mt Maggrom is perplexing. They must have had contact with some organization back in Eastbourne who could supply them. Given your political inclinations and knowledge, are you aware of any such organizations?

I wish you the best on your expedition.

M.