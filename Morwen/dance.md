## Contemporary

- refined, artisanal, luxury
- controlled
- thought-provoking
- story-telling
	- escape a horrible world to a fantasy, then come back
	- facing death
	- escape and death of a monster
	- princess facing the ugliness of the world

## Burlesque

- femme fatale 
- confidence
- sexual

## Outfits

- completely covered with white and scintillating dress, uncover to reveal

## Moves

- whip hair and change color
- control wind and light with Thaumaturgy and Dancing lights. Cause tremors, disrupts flames, ominous whispers, 
- change outfit