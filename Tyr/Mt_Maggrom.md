# Mt Maggrom

- City run by Halfling
- Used to be the central trading hub.
	- Facilitated by tunnels under the mountains
- Mt Maggrom suffered the least from the Fall. 
- It is a tiered city:
	- Bottom: workers, tradesmen, everyday folk.
	- Middle: markets, producers
	- Top: Rich merchants partiers. Administrators.

## Places

- Flock and Tallier: Scrolls and Potions shop
	- Second level
- Golden Goose: Bar
	- Near [Black Hand](./Black_Hand.md) warehouse
	- Lower level
	- Has bathouse in the back
- The Rolling Boulder: Tavern and Casino in the top ring
- The Sewn Rose: Fancy clothes shop. 2nd ring.
	- Edmund