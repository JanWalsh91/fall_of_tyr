# Tyr

## Tyr - The World

Trade shut down 50 years ago, after the death of King of Amlun, Ulrich. A old steward has taken care of the throne since 50 years, but everyone is trying to get a claim to throne. Diplomacy broke down. All kingdoms suffered drastically due to lack of magic.

Mount Maggrom suffered the least. It has huge stockpiles. Halflings are crafty tradesmen. Halfling throne somewhat forgiven. Other cities end up trading with them.


## Cities

- [Mt Maggrom](./Mt_Maggrom.md)
- [Amlon](./Amlon.md)