# Black Hand

Soon-to-be revived trading organization in a poor warehouse at the edge of a cliff on the lower level of Mt Maggrom. 

## People

- Mr B
	- Could be contacted by Jebb's sending stone, but otherwise hard to get a hold of.
- Mr Jebb Hethrows
	- Accountant/face of Mr B.
	- Halfing
	- Had a pet frog called Mr Grumps whom he cared for deeply
- Mr Veeie
	- Lich who's lived thousands of years
	- Lives in the basement building trinkets, wands, mundane and magical items
	- Artificer

## History

Hired Sword and Shield. 

Used to be called the Black Hand was one of the major trade organizations before the Fall. They were primarily focused on guarding trade caravans and were unscrupulous about clients. This role is now taken over by Guild of Shields.

